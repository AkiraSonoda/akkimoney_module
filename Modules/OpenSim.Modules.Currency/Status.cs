﻿using System;
namespace OpenSim.Modules.Currency {
    public enum Status {
        SUCCESS_STATUS = 0,
        PENDING_STATUS = 1,
        FAILED_STATUS = 2
    }
}
